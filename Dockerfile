FROM ubuntu:18.04
RUN apt update -y && apt install nginx -y
COPY . /var/www/html
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]


